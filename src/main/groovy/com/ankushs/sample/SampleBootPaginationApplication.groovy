package com.ankushs.sample

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties

@EnableConfigurationProperties
@SpringBootApplication
class SampleBootPaginationApplication {

	static void main(String[] args) {
		SpringApplication.run SampleBootPaginationApplication, args
	}
}
