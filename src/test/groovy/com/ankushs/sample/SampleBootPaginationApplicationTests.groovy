package com.ankushs.sample

import com.ankushs.sample.domain.Person
import com.ankushs.sample.repository.PersonRepository
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner

@ActiveProfiles
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner)
class SampleBootPaginationApplicationTests {
	
	@Autowired
	PersonRepository personRepository
	 
	@Test
	void contextLoads() { 
		def person 
		(1..20).each{
			person = new Person(name:"John $it", age : 22)
			personRepository.save(person)
		}
	}
}
