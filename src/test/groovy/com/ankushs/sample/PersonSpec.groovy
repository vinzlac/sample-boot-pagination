package com.ankushs.sample

import com.ankushs.sample.domain.Person
import com.ankushs.sample.repository.PersonRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification

@ActiveProfiles
@SpringBootTest
class PersonSpec extends Specification{
	
	@Autowired
	PersonRepository personRepository
	
	def setup(){
		(1..20).each{
			personRepository.save(new Person(name:"John $it", age : 22))
		}
	}
	
	def ""(){
		given : 
			int x =0
		expect :
			x==0
	}

}
